module.exports = function(grunt) {
  grunt.initConfig({
    clean: ['build'], 
    mkdir: {
      build: {
        options: {
          create: ['build']
        }
      }
    }, 
    coffeeify: {
      options: {},
      build: {
        files: [
          {src:['src/background.coffee'], dest:'build/background.js'}
        ]
      }
    },
  });

  grunt.loadNpmTasks('grunt-coffeeify');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');

  grunt.registerTask('default', ['clean', 'mkdir', 'coffeeify']);
}

